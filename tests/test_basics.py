import unittest
from flask import current_app, url_for
from app import create_app, db
from base64 import b64encode
import json

class BasicsTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def tearDown(self):
        db.session.remove()
        self.app_context.pop()

    # --- Basic Tests

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(self.app.config['TESTING'])

    def test_app_schema(self):
        self.assertEqual(self.app.config['SCHEMA_NAME'], self.app.config['SCHEMA_NAME_TESTING'])

    def test_404(self):
        response = self.client.get('/wrong/url', content_type='application/json')
        self.assertTrue(response.status_code == 404)

    def test_index(self):
        response = self.client.get(url_for('main.index'), content_type='application/json')
        self.assertTrue(response.status_code == 302)
