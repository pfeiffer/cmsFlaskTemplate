import socket
import argparse
import sys
import subprocess

PRODUCTION_LEVELS = {
    'vocms0150.cern.ch': 'test',
    'vocms0145.cern.ch': 'dev',
    'vocms0146.cern.ch': 'int',
    'vocms0226.cern.ch': 'pro',
    'vocms010.cern.ch' : 'pro', # cmdDbAccess, cmsDbUpload
}

def run_subprocess(params):
    """ Executes subprocess command """
    try:
        return subprocess.check_output(params)
    except subprocess.CalledProcessError as e:
        return 'None'

def get_git_info():
    """ Returns git latest commit info """
    return {
        'tag': run_subprocess(["git", "describe", "--abbrev=0"]),
        'commit': run_subprocess(["git", "rev-parse", "--short", "HEAD"])
    }

def get_hostname():
    """ Returns current hostname """
    return socket.gethostname()

def get_production_level():
    """ Returns the production level of current hostname. If the hostname is not found, returns 'private' """
    hostName = get_hostname()
    level = 'private'
    try:
        level = PRODUCTION_LEVELS[hostName]
    except:
        pass
    return level

def get_config():
    """ Returns current cofig which depends on production level """
    config = 'default'
    prod_level = get_production_level()

    if prod_level in ['int', 'pro']:
        config = 'production'
    return config

def get_secrets(name):
    """ Returns secret configs for the application """
    SECRETS_PATH = '/data/secrets/'
    sys.path.append(SECRETS_PATH)
    if name == 'dbAccess':
        import dbAccess
        return dbAccess.config[name]
    import secrets
    return secrets.secrets[name]

def get_listening_port():
    """ Returns the port number that is set in the keepers config file """
    parser = argparse.ArgumentParser()
    parser.add_argument('--listeningPort', type=int)
    opts, remaining = parser.parse_known_args()
    port = opts.listeningPort
    if not port:
        port = 9999
    return port
