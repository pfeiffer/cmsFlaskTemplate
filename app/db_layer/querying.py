""" File holds all db querying functions """

from sqlalchemy import func, and_, or_, desc, asc
from ..models import GlobalTag, Tag, TagLog, IOV, Payload, GlobalTagMap, Record, RecordReleases, ParsedReleases, GlobalTagMapRequest, ScenarioWorkflowSynchMap, O2ORun, O2OJobs, O2OErrors
from ..utils import utils
from .. import db
from ..services import get_config
from ..auth import cern_ldap
from config import config
from datetime import datetime
import logging

conf_file = get_config()
conf = config[conf_file]
logger = logging.getLogger(conf.LOGGER_NAME)

def get_gt_tags(database, gt):
    """ Returns all tags of specific global tag """
    db.choose_db(database)
    tags = db.session.query(GlobalTagMap).filter_by(global_tag_name=gt).all()
    return GlobalTagMap.to_datatables(tags)
