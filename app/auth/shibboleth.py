""" Common code for getting information from the Shibboleth headers. """

from flask import request
from config import config

conf = config['default']

if conf.PROD_LEVEL == 'private':
    import getpass
    from . import cern_ldap

    user = getpass.getuser()
    user_info = cern_ldap.CERNLDAP().get_user_info(user)
    private_defaults = {
        'Adfs-Login': user,
        'Adfs-Personid': user_info['employeeID'][0],
        'Adfs-Email': user_info['mail'][0],
        'Adfs-Fullname': user_info['displayName'][0],
        # FIXME: Looks like LDAP does not return all the groups.
        'Adfs-Group': ';'.join([x.split('CN=')[1].split(',')[0] for x in user_info['memberOf']]),
    }

def get_id():
    if conf.PROD_LEVEL == 'private':
        return private_defaults['Adfs-Personid']
    return request.headers.get('Adfs-Personid')

def get_username():
    if conf.PROD_LEVEL == 'private':
        return private_defaults['Adfs-Login']
    return request.headers.get('Adfs-Login')

def get_email():
    if conf.PROD_LEVEL == 'private':
        return private_defaults['Adfs-Email']
    return request.headers.get('Adfs-Email')

def get_full_name():
    if conf.PROD_LEVEL == 'private':
        return private_defaults['Adfs-Fullname']
    return request.headers.get('Adfs-Fullname')

def get_groups():
    if conf.PROD_LEVEL == 'private':
        return set(private_defaults['Adfs-Group'].split(';'))
    return set(request.headers.get('Adfs-Group').split(';'))
