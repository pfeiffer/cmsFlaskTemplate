from flask import render_template, redirect, request, url_for, flash, current_app
from flask_login import login_user, logout_user, login_required, current_user, UserMixin
from . import auth, shibboleth
from .. import login_manager

@auth.before_app_request
def before_request():
    if current_user.is_anonymous \
            and request.endpoint \
            and request.endpoint[:5] != 'auth.' \
            and request.endpoint != 'static':
        return redirect(url_for('auth.login', next=request.path))

@auth.route('/login')
def login():
    id = shibboleth.get_id()
    user = User.get(id)
    if user is not None:
        login_user(user)
        return redirect(request.values.get('next') or url_for('main.index'))
    else:
        return redirect("https://login.cern.ch/adfs/ls/?wa=wsignout1.0")

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect("https://login.cern.ch/adfs/ls/?wa=wsignout1.0")


class UserNotFoundError(Exception):
    pass

class User(UserMixin):
    def __init__(self, id):
        if id != shibboleth.get_id():
            raise UserNotFoundError()
        self.id = shibboleth.get_id()
        self.username = shibboleth.get_username()
        self.admin = True if current_app.config['ADMIN_GROUP'] in shibboleth.get_groups() else False
        self.email = shibboleth.get_email()
        self.full_name = shibboleth.get_full_name()

    @classmethod
    def get(self_class, id):
        try:
            return self_class(id)
        except UserNotFoundError:
            return None

@login_manager.user_loader
def load_user(id):
    return User.get(id)
